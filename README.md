[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687151.svg)](https://doi.org/10.5281/zenodo.4687151)

# Industrial Sites: Industry Benchmarks 

In this repository is published benchmark data on heating and cooling demand and excess heat potentials of industrial processes as a generic database.

## Repository structure

Files:
```
data/IndustryBenchmarks.csv                                 -- Benchmarks on heating and cooling demand and excess heat potentials for industrial processes
datapackage.json                                            -- Datapackage JSON file with the main meta-data

```

## Documentation
The dataset in the folder "industrial_sites" analyses the industrial Heating and Cooling energy demand and comprises of three mail elements:

**Table 1.** Characteristics of data provided within Task 2.4 Industrial Processes.
<table>
   <tr>
    <td>Task</td>
    <td>Dataset</td>
  </tr>
  <tr>
    <td>1. Performance and cost data of industrial steam and district heating generation technologies</td>
    <td>CAPEX, OPEX, Lifetime, ThermalEfficiency, PowertoHeatRatio</td>
  </tr>
  <tr>
    <td>2. Benchmarks on H&C Final Energy Consumption (FEC) and excess heat potentials for industrial processesIndustryBenchmarks</td>
    <td>IndustryBenchmarks</td>
  </tr>
  <tr>
    <td>3. Industrial plants FEC and excess heat potentials</td>
    <td>IndustrialDatabase</td>
  </tr>
</table>

The datasets are to be found in the respective repository.

## Description of the task
The developed dataset distinguishes more than 60 individual processes/products from the basic materials industry. 
These comprise the production of the most energy-intensive products.

For each process, the following information is included in the dataset:
-	Specific FEC (fuel/electricity) [GJ/t]
-	Share of FEC used for H&C
-	Share of temperature level in H&C distinguishing 8 temperature levels in total
-	Excess heat potentials as share of fuels/electricity consumption distinguishing three temperature levels.



**Table 2.** Industrial processes included in this dataset.
<table>
  <tr>
    <td>Subsector</td>
    <td>Process</td>
    <td>Subsector</td>
    <td>Process</td>
  </tr>
 <tr>
    <td>Iron and steel</td>
    <td>Sinter</td>
    <td>Pulp and paper</td>
    <td>Paper</td>
 </tr>
   <tr>
    <td> </td>
    <td>Blast furnace</td>
    <td> </td>
    <td>Chemical pulp</td>
 </tr>
 <tr>
    <td> </td>
    <td>Electric arc furnace</td>
    <td> </td>
    <td>Mechanical pulp</td>
  </tr>
  <tr>
    <td> </td>
    <td>Rolled steel</td>
    <td> </td>
    <td>Recovered fibres</td>
  </tr>
  <tr>
    <td> </td>
    <td>Coke oven</td>
    <td>Chemicals</td>
    <td>Adipic acid</td>
  </tr>
  <tr>
    <td> </td>
    <td>Smelting reduction</td>
    <td> </td>
    <td>Ammonia</td>
  </tr>
  <tr>
    <td> </td>
    <td>Direct reduction</td>
    <td> </td>
    <td>Calcium carbide</td>
  </tr>
  <tr>
    <td>Non-ferrous metals</td>
    <td>Aluminium, primary</td>
    <td> </td>
    <td>Carbon black</td>
  </tr>
  <tr>
    <td> </td>
    <td>Aluminium, secondary</td>
    <td> </td>
    <td>Chlorine, diaphragma</td>
  </tr>
  <tr>
    <td> </td>
    <td>Aluminium extruding</td>
    <td> </td>
    <td>Chlorine, membrane</td>
  </tr>
  <tr>
    <td> </td>
    <td>Aluminium foundries</td>
    <td> </td>
    <td>Chlorine, mercury</td>
  </tr>
  <tr>
    <td> </td>
    <td>Aluminium rolling</td>
    <td> </td>
    <td>Ethylene</td>
  </tr>
  <tr>
    <td> </td>
    <td>Copper, primary</td>
    <td> </td>
    <td>Methanol</td>
  </tr>
  <tr>
    <td> </td>
    <td>Copper, secondary</td>
    <td> </td>
    <td>Nitric acid</td>
  </tr>
  <tr>
    <td> </td>
    <td>Copper further treatment</td>
    <td> </td>
    <td>Oxygen</td>
  </tr>
  <tr>
    <td> </td>
    <td>Zinc, primary</td>
    <td> </td>
    <td>Polycarbonates</td>
  </tr>
  <tr>
    <td> </td>
    <td>Zinc, secondary</td>
    <td> </td>
    <td>Polyethylene</td>
  </tr>	
  <tr>
    <td>Non-metallic minerals</td>
    <td>Container glass</td>
    <td> </td>
    <td>Polypropylene</td>
  </tr>
  <tr>
    <td> </td>
    <td>Flat glass</td>
    <td> </td>
    <td>Polysulfones</td>
  </tr>
  <tr>
    <td> </td>
    <td>Fibre glass</td>
    <td> </td>
    <td>Soda ash</td>
  </tr>
  <tr>
    <td> </td>
    <td>Other glass</td>
    <td> </td>
    <td>TDI</td>
  </tr>
  <tr>
    <td> </td>
    <td>Houseware, sanitary ware</td>
    <td> </td>
    <td>Titanium dioxide</td>
  </tr>
  <tr>
    <td> </td>
    <td>Technical, other ceramics</td>
    <td>Food, tobacco</td>
    <td>Sugar</td>
  </tr>
  <tr>
    <td> </td>
    <td>Tiles, plates, refractories</td>
    <td> </td>
    <td>Dairy</td>
  </tr>
  <tr>
    <td> </td>
    <td>Clinker Calcination-Dry</td>
    <td> </td>
    <td>Brewing</td>
  </tr>
  <tr>
    <td> </td>
    <td>Clinker Calcination-Semidry</td>
    <td> </td>
    <td>Meat processing</td>
  </tr>
  <tr>
    <td> </td>
    <td>Clinker Calcination-Wet</td>
    <td> </td>
    <td>Bread & bakery</td>
  </tr>
  <tr>
    <td> </td>
    <td>Preparation of limestone</td>
    <td> </td>
    <td>Starch</td>
  </tr>
  <tr>
    <td> </td>
    <td>Gypsum</td>
    <td>Others</td>
    <td>Plastics: Extrusion</td>
  </tr>
  <tr>
    <td> </td>
    <td>Cement grinding</td>
    <td> </td>
    <td>Plastics: Injection moulding</td>
  </tr>
  <tr>
    <td> </td>
    <td>Lime milling</td>
    <td> </td>
    <td>Plastics: Blow moulding</td>
  </tr>
  <tr>
    <td> </td>
    <td>Bricks</td>
    <td> </td>
    <td></td>
  </tr>
  <tr>
    <td> </td>
    <td>Lime burning</td>
    <td> </td>
    <td></td>
  </tr>
</table>

### Methodology
All process specific data is collected without country distinction. 
On the one side, there is hardly liable information on country differences e.g. in the specific FEC of such processes 
and on the other side differences are expected to be of low importance, because plants are owned by large multinational companies, 
technology providers are global and apply similar technologies across different countries.

**Table 3.** Definition of temperature levels for process cooling and process heating.
<table>
  <tr>
    <td>End-use</td>
    <td>Temperature level</td>
    <td>Comment</td>
  </tr>
 <tr>
    <td>Process cooling</td>
    <td>< - 30°C</td>
    <td>Mostly air separation in chemical industry</td>
  </tr>
  <tr>
    <td> </td>
    <td>- 30-0 °C</td>
    <td>Mostly refrigeration in food industry</td>
  </tr>
  <tr>
    <td> </td>
    <td>0-15 °C</td>
    <td>Mostly cooling in food industry</td>
  </tr>
  <tr>
    <td>Process heating</td>
    <td><100°C</td>
    <td>Low temperature heat (hot water) used in food industry and others</td>
  </tr>
  <tr>
    <td> </td>
    <td>100-200 °C</td>
    <td>Steam, of which much is in paper, food and chemical industry</td>
  </tr>
  <tr>
    <td> </td>
    <td>200-500 °C</td>
    <td>Steam used mostly in chemical industry</td>
  </tr>
  <tr>
    <td> </td>
    <td>500-1000 °C</td>
    <td>Industrial furnaces mainly in chemical industry</td>
  </tr>
  <tr>
    <td> </td>
    <td>>1000 °C</td>
    <td>Industrial furnaces in steel, cement, glass and other industries</td>
  </tr>
</table>

### Limitations of data

The data set is generally based on literature values, and was compared and benchmarked with energy balances. 
Still, there is substantial uncertainty in the individual values. 
Uncertainty not only results from the quality of data sources, but also is a result of aggregation and industrial structure. 
E.g. the paper production is aggregated as an average process "paper". 
However, in reality, the product mix varies across the countries with different shares for newsprint, hygienic paper, graphic paper etc. 
Specific FEC of these paper grades might range by +/- 50% around the average value included in the data set.

## References
Sources are inidcated process-specific in the dataset.

## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 

### Authors

Tobias Fleiter, Pia Manz <sup>*</sup>

<sup>*</sup> [Fraunhofer Institute for Systems and Innovation Research](https://www.isi.fraunhofer.de/en.html)  
Breslauer Str. 48  
D-76139 Karlsruhe

### License

Copyright © 2016-2018: Tobias Fleiter, Pia Manz
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) 
(Grant Agreement number 723677), which provided the funding to carry out the present investigation.
